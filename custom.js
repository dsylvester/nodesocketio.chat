var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var counter = 1;
var users = [];

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

app.use('/scripts', express.static(__dirname + '/node_modules/'));



io.on('connection', function(socket){
  console.log('a user connected');
  
  
  
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
  
  socket.on('name', function(name){
    users.push(name);
    console.log(users);
    socket.emit('chat message', name +  " has joined!");
    AnnounceNewUser(socket, name);
    
  socket.emit('chat message', name +  " has joined!");
  });
  
  io.on('chat message', function(msg){
    console.log('message: ' + msg);
    io.emit('chat message', msg);
  });
});



function AnnounceNewUser(io, name){
  io.emit('chat message', name +  " has joined!");
  
  counter++;
  console.log(users);
  
}

function RemoveUserOnDisconnect(){
  
}